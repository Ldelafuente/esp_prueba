/* Copyright 2020, TD2-FRH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#define _GNU_SOURCE
#include <esp8266.h>
#include <stdint.h>
#include <main.h>
#include <stdint.h>
#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/

/** @brief tick counter */
static uint32_t counter_ms;
static MODULO_STATES_T state;
static uint8_t rx_buf[256];
static uint8_t tx_buf[256];
static uint8_t txdone;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
static void clean_rx_buffer(void) {
	huart1.pRxBuffPtr = rx_buf;
	huart1.RxXferSize = 256;
	huart1.RxXferCount = 256;
	bzero(rx_buf, 256);
	bzero(tx_buf, 256);

}
/*==================[external functions definition]==========================*/

void esp8266_init(void)
{
	HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);												//Puedo utilizar un hal delay porque estoy fuera del superloop
	HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, GPIO_PIN_SET);
	state=ESTADO_0;
}


//Este callback se ejecuta cuando huart transmitit termina de enviar.
//uso a txdone como si fuese un evento que levanta a 1 la variable y me "avisa" que termino de mandar
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	txdone=1;
}


void esp8266_loop(void)
{
	HAL_UART_Receive_IT(&huart1, rx_buf, 256);
	switch (state)
	{
		case ESTADO_0:
			if(memmem(rx_buf,256,(uint8_t*)"ready\r\n",7))
			{
				state= ESTADO_1;
				txdone=0;
				clean_rx_buffer();
				HAL_UART_Transmit_IT(&huart1, (uint8_t*)"AT+CWMODE=1\r\n", 13);

			}
			break;

		case ESTADO_1:
			if(txdone==1)
			{
				state=ESTADO_2;
				txdone=0;
			}
			break;

		case ESTADO_2:
			if(memmem(rx_buf,256,(uint8_t*)"OK\r\n",4))
			{
				state=ESTADO_3;
				txdone=0;
				HAL_UART_Transmit_IT(&huart1, (uint8_t*)"AT+CWJAP=\"celularr\",\"12345678\"\r\n",strlen("AT+CWJAP=\"celularr\",\"12345678\"\r\n"));
				clean_rx_buffer();
			}
			break;

		case ESTADO_3:
			if( memmem(rx_buf,256,(uint8_t*)"OK\r\n",4)&&(txdone==1))
			{
				state=ESTADO_4;
				txdone=0;
				clean_rx_buffer();
				HAL_UART_Transmit_IT(&huart1, (uint8_t*) "AT+CIPSTART=\"UDP\",\"192.168.43.1\",8000,8001\r\n",strlen("AT+CIPSTART=\"UDP\",\"192.168.43.1\",8000,8001\r\n"));
			}
			break;

		case ESTADO_4:
			if( memmem(rx_buf,256,(uint8_t*)"OK\r\n",4)&&(txdone==1))
			{
				state=ESTADO_5;
				clean_rx_buffer();
				txdone=0;
			}
			break;

		case ESTADO_5:
			snprintf((char *) tx_buf, 256, "AT+CIPSEND=%d\r\n",strlen("ESTO ES BOOOOOOOOOOCA\r\n"));
			HAL_UART_Transmit_IT(&huart1, (uint8_t*) tx_buf,strlen((char*) tx_buf));
			state=ESTADO_6;
			break;

		case ESTADO_6:
			if( memmem(rx_buf,256,(uint8_t*)">",1)&&(txdone==1))
			{
				txdone=0;
				state=ESTADO_7;
				clean_rx_buffer();
				HAL_UART_Transmit_IT(&huart1, (uint8_t*) "ESTO ES BOOOOOOOOOOCA\r\n",strlen("ESTO ES BOOOOOOOOOOCA\r\n"));
		}

			break;

		case ESTADO_7:

			if (memmem(rx_buf, 256, (uint8_t*) "SEND OK", 7)&&(txdone==1))
			{
				clean_rx_buffer();
				state = ESTADO_8;
				counter_ms = 1000;
			}

			break;

		case ESTADO_8:
			if (counter_ms == 0)
			state=ESTADO_5;
			break;


	}


}

void esp8266_tick(void)
{
    /* TODO: tick handler. Keep as short as possible! */
	if(counter_ms>0)
	{
    counter_ms--;
	}
}
/*==================[end of file]============================================*/
